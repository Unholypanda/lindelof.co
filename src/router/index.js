import Vue from 'vue'
import Router from 'vue-router'
import HomePage from '@/components/HomePage'
import Resume from '@/components/Resume'
import Blog from '@/components/Blog'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'HomePage',
      component: HomePage
    },
    {
      path: '/resume/',
      name: 'Resume',
      component: Resume
    },
    {
      path: '/blog/',
      name: 'Blog',
      component: Blog
    }
  ]
})
