// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import NavBar from '@/components/NavBar'
import Footer from '@/components/Footer'
import router from './router'

Vue.config.productionTip = false

Vue.component('nav-bar', NavBar)
Vue.component('page-footer', Footer)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { NavBar, App, Footer },
  template: '<App/>'
})
